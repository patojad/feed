import React from 'react';
import Parser from 'rss-parser';

import './CardFeed.css';

class CardFeed extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      blog: undefined
    }
  }

  componentDidMount(){
    let parser = new Parser();
    const {url} = this.props;
    const CORS_PROXY = "https://cors-anywhere.herokuapp.com/"
    parser.parseURL(CORS_PROXY + url)
      .then(blog => {
        this.setState({ blog });
      })
      .catch(err => {
        console.log("Error en CardFeed");
        console.log(err);
      })
  }

  renderPosts(posts){
    return(
      posts.slice(0,5).map(function(post){
        return(
          <a href={post.link} target="_blank" rel="noopener noreferrer">
            <li className="list-group-item text-center">
              {post.title}
            </li>
          </a>
        )
      })
    )
  }

  render() {
    const { blog } = this.state;
    return(
      blog ?
      <div className="col-md-6 precard">
        <div className="card transparent text-white">

          <div className="card-header text-center">
            {blog.title}
          </div>

          <div className="card-body white-text">
            <ul className="list-group list-group-flush">
              {this.renderPosts(blog.items)}
            </ul>
          </div>

        </div>
      </div> : <div />
    );
  }

}

export default CardFeed;
