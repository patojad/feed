import React from 'react';
import {Link} from "react-router-dom";

import './CardSite.css';

class CardSite extends React.Component {
  blog;
  url;

  constructor(props, context) {
    super(props, context);
    this.blog = this.props.blog
    this.url = {
      pathname: "/site/".concat(this.blog.id)
    }
  }


  render() {
    return(
      <div className="col-md-3 precard">
        <Link to={this.url}>
          <div className="card transparent text-white">
            <div className="card-header text-center">
              {this.blog.name}
            </div>

            <div className="card-body white-text">
              <img src={this.blog.img} className="img-fluid" />
            </div>
          </div>
        </Link>
      </div>
    );
  }

}

export default CardSite;
