import React from 'react';
import {Link} from "react-router-dom";

import './NavBar.css';

function NavBar() {

  return (
    <nav className="navbar navbar-expand-lg navbar-dark transparent">

      <a className="navbar-brand" href="/">Feed</a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation">
        <i className="fa fa-bars" aria-hidden="true"></i>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav ml-auto">

          <li className="nav-item active">
            <Link className="nav-link" to="/">
              <i className="fa fa-home" aria-hidden="true"></i>
            </Link>
          </li>
          <li className="nav-item active">
            <Link className="nav-link" to="/sites">
              <i class="fa fa-globe-w" aria-hidden="true"></i>
            </Link>
          </li>

        </ul>
      </div>
    </nav>
  );
}

export default NavBar;
