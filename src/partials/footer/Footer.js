import React from 'react';

import './Footer.css';

class Footer extends React.Component {



  render(){
    return (
      <footer className="page-footer transparent text-white">
        <div className="container-fluid">
          <div className="row">
            <div className="col l6 m12">
              <h5 className="white-text">Sobre Feed</h5>
              <br />
              <p className="grey-text text-lighten-4">
                Feed es un servicio que junta los RSS de multiples sitios web con el fin de mantener informados a los lectores ingresando en un solo lugar. Cualquiera puede agregarse a este servicio hablandonos en nuestro grupo <a
                  className="grey-text text-lighten-3"
                  href="https://t.me/PatoJADCommunity"
                  target="_blank"
                  rel="noopener noreferrer">
                  Telegram
                </a>.
              </p>
            </div>
            <div className="col l3 m12 s12">
              <center>
                <h5 className="white-text">Redes Sociales</h5>
                <br />
                <ul className="ul-footer">
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://facebook.com/PatoJAD/"
                      target="_blank"
                      rel="noopener noreferrer">
                      Facebook
                    </a>
                  </li>
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://twitter.com/PatoJADOficial"
                      target="_blank"
                      rel="noopener noreferrer">
                      Twitter
                    </a>
                  </li>
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://www.youtube.com/channel/UCta4Iy4TzMx8Xo0pwpkbWgg"
                      target="_blank"
                      rel="noopener noreferrer">
                      Youtube
                    </a>
                  </li>
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://www.twitch.tv/patojad"
                      target="_blank"
                      rel="noopener noreferrer">
                      Twitch
                    </a>
                  </li>
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://mastodon.social/@PatoJAD"
                      target="_blank"
                      rel="noopener noreferrer">
                      Mastodon
                    </a>
                  </li>
                </ul>
              </center>
            </div>
            <div className="col l3 m12 s12">
              <center>
                <h5 className="white-text">Otros Sitios</h5>
                <br />
                <ul className="ul-footer">
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://patojad.com.ar"
                      target="_blank"
                      rel="noopener noreferrer">
                      Blog de Linux
                    </a>
                  </li>
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://pensamientos.patojad.com.ar"
                      target="_blank"
                      rel="noopener noreferrer">
                      Pensamientos
                    </a>
                  </li>
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://ytplayer.patojad.com.ar"
                      target="_blank"
                      rel="noopener noreferrer">
                      YTPlayer
                    </a>
                  </li>
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://compartelibre.patojad.com.ar"
                      target="_blank"
                      rel="noopener noreferrer">
                      Comparte Libre
                    </a>
                  </li>
                  <li>
                    <a
                      className="grey-text text-lighten-3"
                      href="https://mediastodon.patojad.com.ar"
                      target="_blank"
                      rel="noopener noreferrer">
                      Mediastodon
                    </a>
                  </li>
                </ul>
              </center>
            </div>
          </div>
        </div>
        <div className="footer-copyright">
          <div className="container">
            <center>
              Feed © 2020 PatoJAD
            </center>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
