import React from 'react'
import './App.css';
import {
  HashRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Home from './pages/home/Home.js';
import Site from './pages/site/Site.js';
import Sites from './pages/sites/Sites.js';
import Footer from './partials/footer/Footer.js';
import NavBar from './partials/navbar/NavBar.js';

function App() {
  return (
    <Router>
      <video autoPlay muted loop className="background-mp4">
        <source src="/img/background.mp4" type="video/mp4" />
      </video>

      <NavBar />

      <Switch>
        <Route path="/site/:id" component={Site} />
        <Route path="/sites" component={Sites} />
        <Route path="/" component={Home} />
      </Switch>

      <Footer />

    </Router>
  );
}

export default App;
