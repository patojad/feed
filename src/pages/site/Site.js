import React from 'react';
import Parser from 'rss-parser';
import MetaTags from 'react-meta-tags';

import './Site.css';

import Blogs from '../../services/sites.json';

class Site extends React.Component {
  blogfid;
  constructor(props) {
    super(props);
    const { match: { params } } = this.props;

    this.state = {
      blog: undefined
    }

    this.blogfid = Blogs.list.find(blog => blog.id == params.id)

  }

  componentDidMount(){
    let parser = new Parser();
    const url = this.blogfid.url;
    const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

    parser.parseURL(CORS_PROXY + url)
      .then(blog => {
        this.setState({ blog });
      })
      .catch(err => {
        console.log("Error en Site");
        console.log(err);
      })
  }

  drawPost(items){
    console.log(items)
    return(
      items.map(function(post){
        return(

            <tr>
              <th scope="row">
                <a href={post.link} target="_blank" rel="noopener noreferrer">
                  {post.title}
                </a>
              </th>
              <td>{post.content.substring(0,99).concat("...")}</td>
              <td>{post.pubDate}</td>
            </tr>

        )
      })
    )
  }


  render() {
    const { blog } = this.state;
    return(
      blog ?
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-8">

            <MetaTags>
              <title>{this.blogfid.name} | Feed</title>
              <meta name="description" content={this.blogfid.name} />
              <meta property="og:title" content={this.blogfid.name} />
              <meta property="og:image" content={this.blogfid.img} />
            </MetaTags>

            <table className="table table-borderless text-center">
              <thead className="thead">
                <tr>
                  <th scope="col">Titulo</th>
                  <th scope="col">Descripcion</th>
                  <th scope="col">Fecha</th>
                </tr>
              </thead>
              <tbody>
                {this.drawPost(blog.items)}
              </tbody>
            </table>

          </div>

          <div className="col-md-4 text-white text-center">
            <h1>{this.blogfid.name}</h1>
            <br />
            <img src={this.blogfid.img} className="img-fluid" />
            <br />
            <a href={this.blogfid.urlsite} className="btn btn-primary btn-sites">
              Ir al Sitio Web
            </a>
            <br />
            <a href={this.blogfid.url} className="btn btn-primary btn-sites">
              Ir al RSS
            </a>
          </div>

        </div>
      </div> : <div />
    );
  }

}

export default Site;
