import React from 'react';


import './Home.css';

import CardFeed from '../../partials/cardfeed/CardFeed.js';
import Blogs from '../../services/sites.json';

class Home extends React.Component {

  drawBlogs(){
    return(Blogs.list.map(function(blog){
      return(<CardFeed url={blog.url} />)
    }))
  }

  render() {
    return(
      <div className="container-fluid">
        <div className="row">
          { this.drawBlogs() }
        </div>
      </div>
    );
  }

}

export default Home;
