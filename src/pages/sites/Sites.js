import React from 'react';


import './Sites.css';

import CardSite from '../../partials/cardsite/CardSite.js';
import Blogs from '../../services/sites.json';

class Sites extends React.Component {

  drawBlogs(){
    return(Blogs.list.map(function(blog){
      return(<CardSite blog={blog} />)
    }))
  }

  render() {
    return(
      <div className="container-fluid">
        <div className="row">
          { this.drawBlogs() }
        </div>
      </div>
    );
  }

}

export default Sites;
